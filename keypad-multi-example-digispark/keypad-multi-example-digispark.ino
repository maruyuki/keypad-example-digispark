#include "DigiKeyboard.h"
unsigned long curr, start, period;
int count0, count2, count5;

void setup() {
  pinMode(0, INPUT_PULLUP); // Using internal pull-up. Contact to GND = input.
  pinMode(2, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP); // It won't work on compatible boards.

  period = 1500; // Count how many times pressed within 1.5s starting from first press.
}

void loop() {
  DigiKeyboard.update();
  countPress();
  int i = count0 + count2; // Checking first button press. If you have Digistump board and want to use pin #5, add count5.
  if (i > 0) {
    curr = millis(); 
    start = curr;
    while ((curr - start) <= period) {
      countPress();
      curr = millis();
    }
  }
  checkCount();
  count0 = 0;
  count2 = 0;
  count5 = 0; // count5 seems always "1" on compatible boards.
}
  

void countPress(){
  if (digitalRead(0) == LOW){
    count0++;
  } else if (digitalRead(2) == LOW){
    count2++;
  } else if (digitalRead(5) == LOW){
    count5++;
  }
  DigiKeyboard.delay(150); // Delay for avoid switch chattering. You may want to tweak this value.
}

void checkCount() {
  if (count0 > 3) {
    count0 = 3; 
  } if (count2 > 3) {
    count2 = 3;
  } if (count5 > 3) {
    count5 = 3;
  }
  if (count0 == 1) {
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("0, 1.");
  } else if (count0 == 2) {
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("0, 2.");
  } else if (count0 == 3) {
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("0, 3.");
  } else if (count2 == 1) {
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("2, 1.");
  } else if (count2 == 2) {
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("2, 2.");
  } else if (count2 == 3) {
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("2, 3.");
  }
  // If you need it. write code for pin #5 here.
}
