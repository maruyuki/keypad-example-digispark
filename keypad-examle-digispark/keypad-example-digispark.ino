#include "DigiKeyboard.h"
#define KEY_ESCAPE 9 // You can define missing keys in DigiKeyboard.h in this way

void setup() {
  /*
   * Digispark comes with 6 (0-5) I/O pins. However:
   * Pin #1 is practically unusable since it's connected to internal LED (some boards use other pin for this purpose).
   * #3 and #4 are used for USB communication.
   * #5 isn't available for use on most compatible boards.
   */
  pinMode(0, INPUT_PULLUP); // Using internal pull-up. Contact to GND = input.
  pinMode(2, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);  
}

void loop(){
  DigiKeyboard.update();
  /*
   * Generate a key stroke ("A" for example):
   * DigiKeyboard.sendKeyStroke(KEY_A);
   * 
   * Generate a combined key stroke (Ctrl + Alt + F1 for example):
   * DigiKeyboard.sendKeyStroke(KEY_F1, MOD_ALT_LEFT | MOD_CONTROL_LEFT);
   * 
   * Input a text:
   * DigiKeyboard.println("text you want to input");
   * 
   * Key codes are defined in DigiKeyboard.h
   * You can also generate mouse input with DigiMouse.h
   */
  if (digitalRead(0) == LOW){
    DigiKeyboard.sendKeyStroke(0); // It's a dummy input for compatibility
    DigiKeyboard.sendKeyStroke(KEY_F1, MOD_ALT_LEFT | MOD_CONTROL_LEFT);
  } else if (digitalRead(2) == LOW){
    DigiKeyboard.sendKeyStroke(0);
    DigiKeyboard.println("text you want to input");
  }
  DigiKeyboard.delay(100);
}
